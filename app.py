"""
This module serves as the main entry point for calculating and displaying 
information from the solar and lunar calendars, as well as identifying 
special days based on astronomical events.
"""
from datetime import datetime
from flask import Flask, jsonify, render_template
from src.solar_calendar import collect_sun_time
from src.lunar_calendar import collect_day_month
from src.special_days import collect_special_day
from src.utils import get_timezone_info
from src.bedtime import isbedtime

app = Flask(__name__)

# Location for De Bilt
LATITUDE = 52.109271
LONGITUDE = 5.180967
LOCAL_TZ  = get_timezone_info(LATITUDE, LONGITUDE)
LANGUAGE = "nl"  # 'nl' for Dutch or 'en' for English


@app.route('/api/data')
def update_data():
    given_datetime = datetime.now(tz=LOCAL_TZ)
    #given_datetime = datetime(2024, 4, 26, 11, 55, tzinfo=LOCAL_TZ)

    current_time, dawn, night_mode = collect_sun_time(
        LATITUDE, LONGITUDE, LOCAL_TZ, LANGUAGE, given_datetime
    )
    lunar_day, lunar_month, lunar_month_number = collect_day_month(
        LOCAL_TZ, LANGUAGE, given_datetime, dawn
    )
    special_event = collect_special_day(
        LOCAL_TZ, LANGUAGE, given_datetime, dawn, lunar_day, lunar_month_number
    )
    bedtime_mode = isbedtime(given_datetime, dawn)

    # Prepare the information to be displayed
    data = {
        'solar_info': current_time,
        'special_event': special_event,
        'lunar_day': lunar_day,
        'lunar_month': lunar_month,
        'night_mode': night_mode,
        'bedtime_mode': bedtime_mode
    }
    # Render a template with the context
    return jsonify(data)


@app.route('/')
def home():
    given_datetime = datetime.now(tz=LOCAL_TZ)
    #given_datetime = datetime(2024, 8, 27, 11, 55, tzinfo=LOCAL_TZ)

    current_time, dawn, night_mode = collect_sun_time(
        LATITUDE, LONGITUDE, LOCAL_TZ, LANGUAGE, given_datetime
    )
    lunar_day, lunar_month, lunar_month_number = collect_day_month(
        LOCAL_TZ, LANGUAGE, given_datetime, dawn
    )
    special_event = collect_special_day(
        LOCAL_TZ, LANGUAGE, given_datetime, dawn, lunar_day, lunar_month_number
    )
    bedtime_mode = isbedtime(given_datetime, dawn)
    # Pass the data to the template
    return render_template('index.html',
        solar_info=current_time, special_event=special_event,
        lunar_day=lunar_day, lunar_month=lunar_month, night_mode=night_mode, bedtime_mode=bedtime_mode)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
